# QUO Backend Developer Test

Crea un backend no monolítico, es decir estructurando 2 microservicios y un
gateway, utiliza GraphQL (preferentemente) para la implementación, o en su
defecto Express, dicha implementación debe realizarse tomando en cuenta los
siguientes lineamientos y requerimientos:

1. Clona el siguiente repositorio:
https://gitlab.com/apps86/backend-developer-test
2. Deberás crear una rama con tu nombre para poder subir la solución.
3. El código referente a cada micro servicio deberá estar en una subcarpeta
dentro del filesystem del gateway como un mono repo (por cuestiones de
simplicidad).
4. El primer micro servicio deberá incluir los endpoints necesarios para
gestionar una colección de Posts mediante un CRUD, la estructura del objeto
Post es la siguiente: 
```json
{
  id: <String>,
  title: <String>,
  content: <String>,
  autor: <String>,
  image: <String>,
  comments: [Comment]
}
```
5. El segundo microservicio se encargará de gestionar la colección de Commets
asociados a cada Post cada objeto de la colección deberá tener la siguiente
estructura: 
```json
{
   id: <String>,
   name: <String>,
   comment: <String>,
   post_id: <String>
}
```
6. El gateway debe agrupar los endpoints de ambos microservicios debido a
que será en único punto de acceso al backend.
7. Para la generación de imágenes dummy puedes utilizar faker js o la
herramienta que comúnmente uses para tal efecto.
8. En cuanto al almacenamiento de datos de prueba integra json-server en los
microservicios.
9. El setup del servicio debe hacerse mediante un contenedor docker por lo cual
es necesario que incluyas un dockerfile con el cual el servicio sería
levantado, la imagen puede ser desarrollada por ti o de un tercero.
10.Para lograr rápidamente el setup del backend mediante un contenedor de
docker debes incluir un archivo docker-compose.yml el cual contenga el
mapeo de cada servicio o servicios con sus configuraciones específicas, tales
como variables de entorno, puertos, etc.
